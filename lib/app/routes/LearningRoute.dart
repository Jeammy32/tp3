import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tp3/app/config.dart';
import 'package:tp3/app/constant.dart';

import '../strings.dart';

class LearningRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LearningRouteState();
}

class LearningRouteState extends State<LearningRoute> {
  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context);
    return Scaffold(
      key: PageStorageKey(Constant.LEARNING_PAGE_KEY),
      appBar: AppBar(
        title: Text(strings.title),
      ),
      body: LearningPage(),
    );
  }
}

class LearningPage extends StatelessWidget {
  LearningPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount:
                MediaQuery.of(context).orientation == Orientation.portrait ? 2 : 3),
        itemCount: Config.graphemes.length,
        itemBuilder: (_, i) {
          return Card(
            elevation: 4.0,
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: ListTile(
                title: Text(
                  "${Config.graphemes[i].symbol}",
                  textScaleFactor: 6.0,
                  textAlign: TextAlign.center,
                ),
                subtitle: Text(
                  "${Config.graphemes[i].translation}",
                  textScaleFactor: 2.0,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
