import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tp3/app/constant.dart';

import '../durations.dart';
import '../strings.dart';
import 'LearningRoute.dart';
import 'TrainingRoute.dart';

class HomeRoute extends StatefulWidget {
  HomeRoute({Key key, this.title}) : super(key: key);

  final String title;

  @override
  State<StatefulWidget> createState() => _HomeRouteState();
}

class _HomeRouteState extends State<HomeRoute> {
  final _pageController = PageController(
    initialPage: 0,
  );

  int _selectedIndex = 0;

  void _pageChanged(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      _pageController.animateToPage(index,
          duration: Durations.pageTransitionDuration, curve: Curves.ease);
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context);
    return Scaffold(
      key: PageStorageKey(Constant.LEARNING_PAGE_KEY),
      appBar: AppBar(
        title: Text(strings.title),
      ),
      body: _buildBody(context),
      bottomNavigationBar: _bottomNavigationBar(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return PageView(
      controller: _pageController,
      onPageChanged: (index) {
        _pageChanged(index);
      },
      children: <Widget>[
        LearningPage(key: PageStorageKey(Constant.LEARNING_PAGE_KEY)),
        TrainingRoute(key: PageStorageKey(Constant.TRAINING_PAGE_KEY))
      ],
    );
  }

  Widget _bottomNavigationBar(BuildContext context) {
    final strings = Strings.of(context);
    return BottomNavigationBar(
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.book),
          title: Text(strings.learn),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.create),
          title: Text(strings.train),
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Colors.blueAccent,
      onTap: _onItemTapped,
    );
  }
}
