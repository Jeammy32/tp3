import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tp3/app/config.dart';

import 'package:tp3/app/models/GraphemeModel.dart';

class TrainingRoute extends StatefulWidget {
  TrainingRoute({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _TrainingRouteState();
}

class _TrainingRouteState extends State<TrainingRoute>
    with AutomaticKeepAliveClientMixin<TrainingRoute> {
  Random random = new Random();

  GraphemeModel response;
  int responseIndex = 0;

  void changeIndex() {
    setState(() => responseIndex = random.nextInt(Config.graphemes.length));
  }

  List<GraphemeModel> choicesButtonList;
  List<bool> choiceButtonClickedList = [true, true, true];

  @override
  // ignore: must_call_super
  Widget build(BuildContext context) {
    response = Config.graphemes[responseIndex];
    setButtonResponse();
    if (MediaQuery.of(context).orientation == Orientation.portrait) {
      return _portrait(context);
    } else {
      return _landscape(context);
    }
  }

  Widget _portrait(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
      child: Column(
        children: <Widget>[
          Expanded(
            child: Card(
              elevation: 4.0,
              child: Center(
                child: Text(
                  "${Config.graphemes[responseIndex].symbol}",
                  textScaleFactor: 16.0,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          raisedButtonChoice(0),
          raisedButtonChoice(1),
          raisedButtonChoice(2),
        ],
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
      ),
    );
  }

  Widget _landscape(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Card(
              elevation: 4.0,
              child: Center(
                child: Text(
                  "${Config.graphemes[responseIndex].symbol}",
                  textScaleFactor: 8.0,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          Expanded(
            child: Column(
              children: <Widget>[
                raisedButtonChoice(0),
                raisedButtonChoice(1),
                raisedButtonChoice(2),
              ],
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
            ),
          )
        ],
      ),
    );
  }

  Widget raisedButtonChoice(int id) {
    return RaisedButton(
      onPressed: () => onPressed(choicesButtonList[id].translation, id),
      child: Text(
        "${choicesButtonList[id].translation}",
        textAlign: TextAlign.center,
      ),
      color: choiceButtonClickedList[id] ? Colors.white : Colors.redAccent,
      elevation: 4.0,
    );
  }

  void onPressed(String translation, int id) {
    if (translation == response.translation) {
      changeIndex();
      resetButton();
    } else {
      setState(() => choiceButtonClickedList[id] = false);
    }
  }

  void resetButton() {
    for (var i = 0; i < choiceButtonClickedList.length; i++) {
      setState(() => choiceButtonClickedList[i] = true);
      fillChoiceArrayWithEmptyGrapheme();
    }
  }

  void setButtonResponse() {
    if (choicesButtonList == null || choicesButtonList[0].translation == null) {
      fillChoiceArrayWithEmptyGrapheme();
      assignResponseToRandomButton();

      for (var i = 0; i < choicesButtonList.length; i++) {
        if (choicesButtonList[i].translation == null) {
          GraphemeModel newGrapheme = getNewRandomGrapheme();
          while (choicesButtonList[i].translation == null) {
            if (!checkIfAlreadyInList(newGrapheme)) {
              choicesButtonList[i] = newGrapheme;
            } else {
              newGrapheme = getNewRandomGrapheme();
            }
          }
        }
      }
    }
  }

  GraphemeModel getNewRandomGrapheme() {
    return Config.graphemes[random.nextInt(Config.graphemes.length)];
  }

  void assignResponseToRandomButton() {
    choicesButtonList[random.nextInt(3)] = response;
  }

  void fillChoiceArrayWithEmptyGrapheme() {
    choicesButtonList = [GraphemeModel(), GraphemeModel(), GraphemeModel()];
  }

  bool checkIfAlreadyInList(GraphemeModel graphemeToCheck) {
    for (var grapheme in choicesButtonList) {
      if (grapheme.translation == graphemeToCheck.translation) {
        return true;
      }
    }
    return false;
  }

  @override
  bool get wantKeepAlive => true;
}
