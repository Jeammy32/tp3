class GraphemeModel {
  final String symbol;
  final String translation;


  const GraphemeModel(
      {String this.symbol, String this.translation}
      );
}
