import 'package:flutter/material.dart';
import 'package:tp3/app/routes/HomeRoute.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:tp3/app/strings.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      supportedLocales: Strings.values.keys.map((it) => Locale(it)),
      theme: ThemeData.light(),
      home: HomeRoute(),
      localizationsDelegates: [
        AppLocalizationsDelegate.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      onGenerateTitle: (BuildContext context) => Strings.of(context).title,
    );
  }
}
